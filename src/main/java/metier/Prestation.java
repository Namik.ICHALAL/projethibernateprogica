package metier;

import java.math.BigDecimal;

public class Prestation {
    private BigDecimal prix;
    private EquipementService equipementService;

    public Prestation(BigDecimal prix) {
        this.prix = prix;
        this.equipementService= equipementService;
    }


    public BigDecimal getPrix() {
        return prix;
    }

    public Prestation setPrix(BigDecimal prix) {
        this.prix = prix;
        return this;
    }

    public EquipementService getEquipementService() {
        return equipementService;
    }

    public Prestation setEquipementService(EquipementService equipementService) {
        this.equipementService = equipementService;
        return this;
    }


}
