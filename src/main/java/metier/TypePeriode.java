package metier;



public class TypePeriode {
    private int idTypePeriode;
    private String libelleTypePeriode;

	public TypePeriode(int idTypePeriode, String libelleTypePeriode) {
		this.idTypePeriode = idTypePeriode;
		this.libelleTypePeriode = libelleTypePeriode;
	}

	public int getIdTypePeriode() {
		return idTypePeriode;
	}

	public TypePeriode setIdTypePeriode(int idTypePeriode) {
		this.idTypePeriode = idTypePeriode;
		return this;
	}

	public String getLibelleTypePeriode() {
		return libelleTypePeriode;
	}

	public TypePeriode setLibelleTypePeriode(String libelleTypePeriode) {
		this.libelleTypePeriode = libelleTypePeriode;
		return this;
	}

	@Override
	public String toString() {
		return libelleTypePeriode ;
	}
}
