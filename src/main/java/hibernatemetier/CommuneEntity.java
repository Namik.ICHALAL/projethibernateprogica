package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "commune", schema = "public", catalog = "gites")
@IdClass(CommuneEntityPK.class)
public class CommuneEntity {
    private String codeDepartement;
    private String codeInsee;
    private String nomCommune;
    private String codePostale;
    private double latitude;
    private double longitude;

    @Id
    @Column(name = "code_departement")
    public String getCodeDepartement() {
        return codeDepartement;
    }

    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    @Id
    @Column(name = "code_insee")
    public String getCodeInsee() {
        return codeInsee;
    }

    public void setCodeInsee(String codeInsee) {
        this.codeInsee = codeInsee;
    }

    @Basic
    @Column(name = "nom_commune")
    public String getNomCommune() {
        return nomCommune;
    }

    public void setNomCommune(String nomCommune) {
        this.nomCommune = nomCommune;
    }

    @Basic
    @Column(name = "code_postale")
    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    @Basic
    @Column(name = "latitude")
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitude")
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommuneEntity that = (CommuneEntity) o;

        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        if (codeDepartement != null ? !codeDepartement.equals(that.codeDepartement) : that.codeDepartement != null)
            return false;
        if (codeInsee != null ? !codeInsee.equals(that.codeInsee) : that.codeInsee != null) return false;
        if (nomCommune != null ? !nomCommune.equals(that.nomCommune) : that.nomCommune != null) return false;
        if (codePostale != null ? !codePostale.equals(that.codePostale) : that.codePostale != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = codeDepartement != null ? codeDepartement.hashCode() : 0;
        result = 31 * result + (codeInsee != null ? codeInsee.hashCode() : 0);
        result = 31 * result + (nomCommune != null ? nomCommune.hashCode() : 0);
        result = 31 * result + (codePostale != null ? codePostale.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
