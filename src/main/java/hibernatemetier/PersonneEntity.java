package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "personne", schema = "public", catalog = "gites")
public class PersonneEntity {
    private int idPersonne;
    private String nomPersonne;
    private String prenomPersonne;
    private String email;

    @Id
    @Column(name = "id_personne")
    public int getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }

    @Basic
    @Column(name = "nom_personne")
    public String getNomPersonne() {
        return nomPersonne;
    }

    public void setNomPersonne(String nomPersonne) {
        this.nomPersonne = nomPersonne;
    }

    @Basic
    @Column(name = "prenom_personne")
    public String getPrenomPersonne() {
        return prenomPersonne;
    }

    public void setPrenomPersonne(String prenomPersonne) {
        this.prenomPersonne = prenomPersonne;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonneEntity that = (PersonneEntity) o;

        if (idPersonne != that.idPersonne) return false;
        if (nomPersonne != null ? !nomPersonne.equals(that.nomPersonne) : that.nomPersonne != null) return false;
        if (prenomPersonne != null ? !prenomPersonne.equals(that.prenomPersonne) : that.prenomPersonne != null)
            return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPersonne;
        result = 31 * result + (nomPersonne != null ? nomPersonne.hashCode() : 0);
        result = 31 * result + (prenomPersonne != null ? prenomPersonne.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
