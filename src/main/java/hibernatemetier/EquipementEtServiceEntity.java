package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "equipement_et_service", schema = "public", catalog = "gites")
public class EquipementEtServiceEntity {
    private int idService;
    private String nomService;

    @Id
    @Column(name = "id_service")
    public int getIdService() {
        return idService;
    }

    public void setIdService(int idService) {
        this.idService = idService;
    }

    @Basic
    @Column(name = "nom_service")
    public String getNomService() {
        return nomService;
    }

    public void setNomService(String nomService) {
        this.nomService = nomService;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EquipementEtServiceEntity that = (EquipementEtServiceEntity) o;

        if (idService != that.idService) return false;
        if (nomService != null ? !nomService.equals(that.nomService) : that.nomService != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idService;
        result = 31 * result + (nomService != null ? nomService.hashCode() : 0);
        return result;
    }
}
