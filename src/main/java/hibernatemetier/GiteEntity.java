package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "gite", schema = "public", catalog = "gites")
public class GiteEntity {
    private int idGite;
    private String nomGite;
    private double surfaceHabitable;
    private Integer nombreChambres;
    private int nombreCouchage;
    private String siteWeb;
    private String adresse1;
    private String adresse2;
    private String libelleVoie;
    private String adresse3;
    private String codePostale;

    @Id
    @Column(name = "id_gite")
    public int getIdGite() {
        return idGite;
    }

    public void setIdGite(int idGite) {
        this.idGite = idGite;
    }

    @Basic
    @Column(name = "nom_gite")
    public String getNomGite() {
        return nomGite;
    }

    public void setNomGite(String nomGite) {
        this.nomGite = nomGite;
    }

    @Basic
    @Column(name = "surface_habitable")
    public double getSurfaceHabitable() {
        return surfaceHabitable;
    }

    public void setSurfaceHabitable(double surfaceHabitable) {
        this.surfaceHabitable = surfaceHabitable;
    }

    @Basic
    @Column(name = "nombre_chambres")
    public Integer getNombreChambres() {
        return nombreChambres;
    }

    public void setNombreChambres(Integer nombreChambres) {
        this.nombreChambres = nombreChambres;
    }

    @Basic
    @Column(name = "nombre_couchage")
    public int getNombreCouchage() {
        return nombreCouchage;
    }

    public void setNombreCouchage(int nombreCouchage) {
        this.nombreCouchage = nombreCouchage;
    }

    @Basic
    @Column(name = "site_web")
    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    @Basic
    @Column(name = "adresse_1")
    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    @Basic
    @Column(name = "adresse_2")
    public String getAdresse2() {
        return adresse2;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    @Basic
    @Column(name = "libelle_voie")
    public String getLibelleVoie() {
        return libelleVoie;
    }

    public void setLibelleVoie(String libelleVoie) {
        this.libelleVoie = libelleVoie;
    }

    @Basic
    @Column(name = "adresse_3")
    public String getAdresse3() {
        return adresse3;
    }

    public void setAdresse3(String adresse3) {
        this.adresse3 = adresse3;
    }

    @Basic
    @Column(name = "code_postale")
    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GiteEntity that = (GiteEntity) o;

        if (idGite != that.idGite) return false;
        if (Double.compare(that.surfaceHabitable, surfaceHabitable) != 0) return false;
        if (nombreCouchage != that.nombreCouchage) return false;
        if (nomGite != null ? !nomGite.equals(that.nomGite) : that.nomGite != null) return false;
        if (nombreChambres != null ? !nombreChambres.equals(that.nombreChambres) : that.nombreChambres != null)
            return false;
        if (siteWeb != null ? !siteWeb.equals(that.siteWeb) : that.siteWeb != null) return false;
        if (adresse1 != null ? !adresse1.equals(that.adresse1) : that.adresse1 != null) return false;
        if (adresse2 != null ? !adresse2.equals(that.adresse2) : that.adresse2 != null) return false;
        if (libelleVoie != null ? !libelleVoie.equals(that.libelleVoie) : that.libelleVoie != null) return false;
        if (adresse3 != null ? !adresse3.equals(that.adresse3) : that.adresse3 != null) return false;
        if (codePostale != null ? !codePostale.equals(that.codePostale) : that.codePostale != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = idGite;
        result = 31 * result + (nomGite != null ? nomGite.hashCode() : 0);
        temp = Double.doubleToLongBits(surfaceHabitable);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (nombreChambres != null ? nombreChambres.hashCode() : 0);
        result = 31 * result + nombreCouchage;
        result = 31 * result + (siteWeb != null ? siteWeb.hashCode() : 0);
        result = 31 * result + (adresse1 != null ? adresse1.hashCode() : 0);
        result = 31 * result + (adresse2 != null ? adresse2.hashCode() : 0);
        result = 31 * result + (libelleVoie != null ? libelleVoie.hashCode() : 0);
        result = 31 * result + (adresse3 != null ? adresse3.hashCode() : 0);
        result = 31 * result + (codePostale != null ? codePostale.hashCode() : 0);
        return result;
    }
}
