package hibernatemetier;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TariferEntityPK implements Serializable {
    private int idGite;
    private int idPeriode;

    @Column(name = "id_gite")
    @Id
    public int getIdGite() {
        return idGite;
    }

    public void setIdGite(int idGite) {
        this.idGite = idGite;
    }

    @Column(name = "id_periode")
    @Id
    public int getIdPeriode() {
        return idPeriode;
    }

    public void setIdPeriode(int idPeriode) {
        this.idPeriode = idPeriode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TariferEntityPK that = (TariferEntityPK) o;

        if (idGite != that.idGite) return false;
        if (idPeriode != that.idPeriode) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idGite;
        result = 31 * result + idPeriode;
        return result;
    }
}
