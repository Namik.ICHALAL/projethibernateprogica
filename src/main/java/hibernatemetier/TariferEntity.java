package hibernatemetier;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tarifer", schema = "public", catalog = "gites")
@IdClass(TariferEntityPK.class)
public class TariferEntity {
    private int idGite;
    private int idPeriode;
    private BigDecimal tarifHebdomadaire;

    @Id
    @Column(name = "id_gite")
    public int getIdGite() {
        return idGite;
    }

    public void setIdGite(int idGite) {
        this.idGite = idGite;
    }

    @Id
    @Column(name = "id_periode")
    public int getIdPeriode() {
        return idPeriode;
    }

    public void setIdPeriode(int idPeriode) {
        this.idPeriode = idPeriode;
    }

    @Basic
    @Column(name = "tarif_hebdomadaire")
    public BigDecimal getTarifHebdomadaire() {
        return tarifHebdomadaire;
    }

    public void setTarifHebdomadaire(BigDecimal tarifHebdomadaire) {
        this.tarifHebdomadaire = tarifHebdomadaire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TariferEntity that = (TariferEntity) o;

        if (idGite != that.idGite) return false;
        if (idPeriode != that.idPeriode) return false;
        if (tarifHebdomadaire != null ? !tarifHebdomadaire.equals(that.tarifHebdomadaire) : that.tarifHebdomadaire != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idGite;
        result = 31 * result + idPeriode;
        result = 31 * result + (tarifHebdomadaire != null ? tarifHebdomadaire.hashCode() : 0);
        return result;
    }
}
