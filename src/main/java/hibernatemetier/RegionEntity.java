package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "region", schema = "public", catalog = "gites")
public class RegionEntity {
    private String codeRegion;
    private String nomRegion;

    @Id
    @Column(name = "code_region")
    public String getCodeRegion() {
        return codeRegion;
    }

    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }

    @Basic
    @Column(name = "nom_region")
    public String getNomRegion() {
        return nomRegion;
    }

    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegionEntity that = (RegionEntity) o;

        if (codeRegion != null ? !codeRegion.equals(that.codeRegion) : that.codeRegion != null) return false;
        if (nomRegion != null ? !nomRegion.equals(that.nomRegion) : that.nomRegion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codeRegion != null ? codeRegion.hashCode() : 0;
        result = 31 * result + (nomRegion != null ? nomRegion.hashCode() : 0);
        return result;
    }
}
