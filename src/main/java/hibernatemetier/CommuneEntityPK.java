package hibernatemetier;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class CommuneEntityPK implements Serializable {
    private String codeDepartement;
    private String codeInsee;

    @Column(name = "code_departement")
    @Id
    public String getCodeDepartement() {
        return codeDepartement;
    }

    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    @Column(name = "code_insee")
    @Id
    public String getCodeInsee() {
        return codeInsee;
    }

    public void setCodeInsee(String codeInsee) {
        this.codeInsee = codeInsee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommuneEntityPK that = (CommuneEntityPK) o;

        if (codeDepartement != null ? !codeDepartement.equals(that.codeDepartement) : that.codeDepartement != null)
            return false;
        if (codeInsee != null ? !codeInsee.equals(that.codeInsee) : that.codeInsee != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codeDepartement != null ? codeDepartement.hashCode() : 0;
        result = 31 * result + (codeInsee != null ? codeInsee.hashCode() : 0);
        return result;
    }
}
