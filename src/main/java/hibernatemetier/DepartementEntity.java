package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "departement", schema = "public", catalog = "gites")
public class DepartementEntity {
    private String codeDepartement;
    private String nomDepartement;

    @Id
    @Column(name = "code_departement")
    public String getCodeDepartement() {
        return codeDepartement;
    }

    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }

    @Basic
    @Column(name = "nom_departement")
    public String getNomDepartement() {
        return nomDepartement;
    }

    public void setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartementEntity that = (DepartementEntity) o;

        if (codeDepartement != null ? !codeDepartement.equals(that.codeDepartement) : that.codeDepartement != null)
            return false;
        if (nomDepartement != null ? !nomDepartement.equals(that.nomDepartement) : that.nomDepartement != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codeDepartement != null ? codeDepartement.hashCode() : 0;
        result = 31 * result + (nomDepartement != null ? nomDepartement.hashCode() : 0);
        return result;
    }
}
