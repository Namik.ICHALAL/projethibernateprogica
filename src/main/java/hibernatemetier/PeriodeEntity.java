package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "periode", schema = "public", catalog = "gites")
public class PeriodeEntity {
    private int idPeriode;
    private int numeroSemaine;

    @Id
    @Column(name = "id_periode")
    public int getIdPeriode() {
        return idPeriode;
    }

    public void setIdPeriode(int idPeriode) {
        this.idPeriode = idPeriode;
    }

    @Basic
    @Column(name = "numero_semaine")
    public int getNumeroSemaine() {
        return numeroSemaine;
    }

    public void setNumeroSemaine(int numeroSemaine) {
        this.numeroSemaine = numeroSemaine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PeriodeEntity that = (PeriodeEntity) o;

        if (idPeriode != that.idPeriode) return false;
        if (numeroSemaine != that.numeroSemaine) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPeriode;
        result = 31 * result + numeroSemaine;
        return result;
    }
}
