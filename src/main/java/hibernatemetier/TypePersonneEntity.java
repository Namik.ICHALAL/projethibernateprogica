package hibernatemetier;

import javax.persistence.*;

@Entity
@Table(name = "type_personne", schema = "public", catalog = "gites")
public class TypePersonneEntity {
    private int idType;
    private String nomType;

    @Id
    @Column(name = "id_type")
    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    @Basic
    @Column(name = "nom_type")
    public String getNomType() {
        return nomType;
    }

    public void setNomType(String nomType) {
        this.nomType = nomType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypePersonneEntity that = (TypePersonneEntity) o;

        if (idType != that.idType) return false;
        if (nomType != null ? !nomType.equals(that.nomType) : that.nomType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idType;
        result = 31 * result + (nomType != null ? nomType.hashCode() : 0);
        return result;
    }
}
