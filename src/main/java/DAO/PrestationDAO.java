package DAO;

import metier.Prestation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PrestationDAO extends  DAO<Prestation> {


    public PrestationDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Prestation getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Prestation> getAll() {
        ResultSet rs;
        ArrayList<Prestation> liste = new ArrayList<Prestation>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  prix from proposer order by  prix";
            Prestation prestation;
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {;
                prestation= new Prestation(rs.getBigDecimal(1));
                liste.add(prestation);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible  "+ error);
        }
        return liste;



    }

    @Override
    public boolean insert(Prestation objet) {
        return false;
    }

    @Override
    public boolean update(Prestation objet) {
        return false;
    }

    @Override
    public boolean delete(Prestation objet) {
        return false;
    }

}
