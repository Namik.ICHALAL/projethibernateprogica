package DAO;


import metier.Tarification;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TarificationDAO extends DAO<Tarification> {


    public TarificationDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Tarification getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Tarification> getAll() {
        ResultSet rs;
        ArrayList<Tarification> liste = new ArrayList<Tarification>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  tarif_hebdomadaire from tarifer order by  tarif_hebdomadaire";
            Tarification tarification;
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {;
                tarification= new Tarification(rs.getBigDecimal(1));
                liste.add(tarification);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données Tarification impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Tarification objet) {
        return false;
    }

    @Override
    public boolean update(Tarification objet) {
        return false;
    }

    @Override
    public boolean delete(Tarification objet) {
        return false;
    }
}
